package com.example.jonatandelgado.appserver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jonatandelgado.appserver.API.APIService;
import com.example.jonatandelgado.appserver.Model.Product;
import com.example.jonatandelgado.appserver.R;

import java.util.ArrayList;

public class Product_Adapter extends BaseAdapter {

    private ArrayList<Product> products;
    private LayoutInflater layoutInflater;

    public Product_Adapter(Context context, ArrayList<Product> products) {
        this.products = products;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return  products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        try {

            view = layoutInflater.inflate(R.layout.activity_row_product, null);
            TextView textName = (TextView) view.findViewById(R.id.textName);
            TextView textUniM = (TextView) view.findViewById(R.id.textUniM);
            TextView txtPrice1 = (TextView) view.findViewById(R.id.txtPrice1);
            TextView txtPrice2 = (TextView) view.findViewById(R.id.txtPrice2);
            TextView txtCategory = (TextView) view.findViewById(R.id.txtCategory);

            textName.setText("Nombre del producto: " + products.get(position).getName());
            textUniM.setText("Unidad de medida: " + products.get(position).getUnit());
            txtPrice1.setText("Precio1: " + String.valueOf( products.get(position).getPrice()));
            txtPrice2.setText("Precio2: " + String.valueOf( products.get(position).getPrice2()));
            txtCategory.setText("Categorias: " + products.get(position).getCategory());

            return view;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
