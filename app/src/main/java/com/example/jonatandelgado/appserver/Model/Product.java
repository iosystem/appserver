package com.example.jonatandelgado.appserver.Model;

public class Product {
    private String name;
    private String unit;
    private double price;
    private double price2;
    private String Category;

    public Product()
    {

    }

    public Product(String name, String unit, double price, double price2, String category) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.price2 = price2;
        Category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice2() {
        return price2;
    }

    public void setPrice2(double price2) {
        this.price2 = price2;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }
}
