package com.example.jonatandelgado.appserver.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.jonatandelgado.appserver.Model.SelfProduct;

import java.util.ArrayList;

public class SelfProduct_Adapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<SelfProduct> selfProducts;

    @Override
    public int getCount() {
        return selfProducts.size();
    }

    @Override
    public Object getItem(int i) {
        return selfProducts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
