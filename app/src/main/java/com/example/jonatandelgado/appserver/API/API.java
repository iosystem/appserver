package com.example.jonatandelgado.appserver.API;

import android.content.Context;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class API {
    protected   String URL_BASE = "http://softgdeveloper.com/";
    protected  String APP_kEY = "?idEstudiante=40220167437";

    public void sendRequest(Context context, JsonObjectRequest jsonRequest)
    {
        Volley.newRequestQueue(context).add(jsonRequest);
    }
}
