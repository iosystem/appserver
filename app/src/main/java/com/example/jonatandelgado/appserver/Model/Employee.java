package com.example.jonatandelgado.appserver.Model;

public class Employee {
    private int id;
    private String lastName;
    private String name;
    private String sectional;
    private String faculty;
    private String salary;
    private String dateBegin;
    private String birthDate;
    private String position;

    public Employee()
    {

    }
    public Employee(int id, String lastName, String name, String sectional,
                    String faculty, String salary, String dateBegin, String birthDate
                    ,String position) {

        this.id = id;
        this.lastName = lastName;
        this.name = name;
        this.sectional = sectional;
        this.faculty = faculty;
        this.salary = salary;
        this.dateBegin = dateBegin;
        this.birthDate = birthDate;
        this.position = position;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSectional() {
        return sectional;
    }

    public void setSectional(String sectional) {
        this.sectional = sectional;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
