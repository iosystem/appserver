package com.example.jonatandelgado.appserver.API;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jonatandelgado.appserver.Adapter.Product_Adapter;
import com.example.jonatandelgado.appserver.Model.Employee;
import com.example.jonatandelgado.appserver.Model.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class APIService extends API {
    private final String REQUEST_PRODUCT = "/GetAllProduts.php";
    private final String REQUEST_CART = "/GetAllCars.php";
    private final String REQUEST_EMPLOYEE = "/GetAllEmployee.php";
    private final String REQUEST_SELF_PRODUCT = "/GetAllData.php";
    private final String REQUEST_SELF_PRODUCT_INSERT = "/Insert_Data.php";

    public  void getServicesEmployee(final  Context context,
                                     final ArrayList<Employee> employees,
                                     final com.example.jonatandelgado.appserver.FrameView.Employee employeeView)
    {
        final JsonObjectRequest jsonRequest =
                new JsonObjectRequest(Request.Method.GET,
                        URL_BASE + REQUEST_EMPLOYEE,
                        null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonProducts = response.getJSONArray("AllEmpleados");
                            for (int i = 0; i < jsonProducts.length(); i++) {
                                JSONArray items = jsonProducts.getJSONArray(i);

                                 int id = Integer.parseInt( items.getString(0));
                                 String lastName = items.getString(1);
                                 String name = items.getString(2);
                                 String sectional = items.getString(3);
                                 String faculty = items.getString(4);
                                 String salary = items.getString(6);
                                 String dateBegin = items.getString(7);
                                 String birthDate = items.getString(8);
                                 String position = items.getString(5);

                                Employee model = new Employee(id,lastName,name,sectional,
                                        faculty,salary,dateBegin,birthDate,position);
                                employees.add(model);
                            }
                            employeeView.refresh();
                        } catch (Exception ex) {
                            Log.v("JSON", "EXC: " + ex.getLocalizedMessage());
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("FUN", "ERROR: " + error.toString());
                    }
                });


        super.sendRequest(context, jsonRequest);
    }

    public void getServiceProduct(final Context context,
                                  final ArrayList<Product> products,
                                  final com.example.jonatandelgado.appserver.FrameView.Product productView
                                  ) {

        final JsonObjectRequest jsonRequest =
                new JsonObjectRequest(Request.Method.GET,
                        URL_BASE + REQUEST_PRODUCT,
                        null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonProducts = response.getJSONArray("AllProducts");
                            for (int i = 0; i < jsonProducts.length(); i++) {
                                JSONArray items = jsonProducts.getJSONArray(i);

                                String nameProduct = items.getString(0);
                                String unidadMedida = items.getString(1);
                                double price1 = items.getDouble(2);
                                double price2 = items.getDouble(3);
                                String category = items.getString(4);

                                Product model = new Product(nameProduct,
                                        unidadMedida, price1, price2, category);
                                products.add(model);
                            }
                            productView.refresh();
                        } catch (Exception ex) {
                            Log.v("JSON", "EXC: " + ex.getLocalizedMessage());
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("FUN", "ERROR: " + error.toString());
                    }
                });

        super.sendRequest(context, jsonRequest);
    }

}
