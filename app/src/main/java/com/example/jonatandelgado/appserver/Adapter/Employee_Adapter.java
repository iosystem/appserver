package com.example.jonatandelgado.appserver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jonatandelgado.appserver.Model.Employee;
import com.example.jonatandelgado.appserver.R;

import java.util.ArrayList;

public class Employee_Adapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<Employee> employees;

    public Employee_Adapter(Context context, ArrayList<Employee> employees)
    {
        this.employees = employees;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return employees.size();
    }

    @Override
    public Object getItem(int i) {
        return employees.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            view = layoutInflater.inflate(R.layout.activity_row_employee, null);
            TextView textIDEmployee = (TextView) view.findViewById(R.id.textIDEmployee);
            TextView textName = (TextView) view.findViewById(R.id.txtNameEmployee);
            TextView textLastName = (TextView) view.findViewById(R.id.textLastName);
            TextView textSectional = (TextView) view.findViewById(R.id.txtSectional);
            TextView textFaculty = (TextView) view.findViewById(R.id.txtFaculty);
            TextView textPosition = (TextView) view.findViewById(R.id.txtPosition);
            TextView textSalary = (TextView) view.findViewById(R.id.txtSalary);
            TextView textStartDate = (TextView) view.findViewById(R.id.txtStartDate);
            TextView textBirthDate = (TextView) view.findViewById(R.id.txtBirthDate);

            textIDEmployee.setText(textIDEmployee.getHint()+String.valueOf(employees.get(i).getId()));
            textName.setText(textName.getHint()+employees.get(i).getName());
            textLastName.setText(textLastName.getHint()+employees.get(i).getLastName());
            textSectional.setText(textSectional.getHint()+employees.get(i).getSectional());
            textFaculty.setText(textFaculty.getHint()+employees.get(i).getFaculty());
            textPosition.setText(textPosition.getHint()+employees.get(i).getPosition());
            textSalary.setText(textSalary.getHint()+employees.get(i).getSalary());
            textStartDate.setText(textStartDate.getHint()+employees.get(i).getDateBegin());
            textBirthDate.setText(textBirthDate.getHint()+employees.get(i).getBirthDate());

            return view;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
