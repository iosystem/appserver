package com.example.jonatandelgado.appserver.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.jonatandelgado.appserver.Model.Car;

import java.util.ArrayList;

public class Car_Adapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<Car> cars;


    @Override
    public int getCount() {
        return cars.size();
    }

    @Override
    public Object getItem(int i) {
        return cars.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
